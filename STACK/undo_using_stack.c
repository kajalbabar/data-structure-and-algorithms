#include <stdio.h>
#define MAX 1000
int top = -1;
int stack[MAX];
int undoPtr;
int redoPtr;
int URlist[MAX], ind=0;

// -------------------- isFull function -------------
int isFull() {
	if(top >= MAX) {
		return 1;
	}
	else {
		return 0;
	}
}
//------------------- isEmpty function -------------------
int isEmpty() {
	if(top == -1)
		return 1;
	else
		return 0;

}

//-------------- push fun ----------------
int push(int num) {

	//if stack is not full 
	if(!isFull()) {
		top++;
		stack[top] = num;
	}
	else {
		return -1;
	}
}


// ---------------------------------------- pop function ----------------------------------
int pop() {
	//if stack is not empty 
	if(!isEmpty()) {
		return stack[top--];
	}
	else
		return 0;
}


//-------------------- display function -----------
void display() {
	if(isEmpty()) {
		printf("Stack is empty!!\n");
	}
	else {
		for(int i=0; i<=top; i++)
			printf("%d\t",stack[i]);
		printf("\n");
	}
}


//undo the operation
int undo() {
	int data = pop();
	if(data != 0)	{
		URlist[ind++] = data;
		return stack[top];
	}
	else {
		return -1;
	}
}


//Redo the operation
int redo() {
	if(ind > 0 ) {
		push(URlist[--ind]);
		return stack[top];
	}
	else {
		return -1;
	}
}


void main() {
	int num,add,choice, status;
	char c;
	printf("Enter input num:");
	scanf("%d",&num);
	push(num);
	int op[MAX],index=0;
	
	do {

		printf("*************** MENU ********************\n");
		printf("1.Add\n");
		printf("2.sub\n");
		printf("3.Display\n");
		printf("4.Undo\n");
		printf("5.Redo\n");
		printf("\nEnter operation: ");
		scanf("%d",&choice);

		switch(choice) {
			case 1:
				printf("Enter num to add: ");
				scanf("%d",&add);
				num = num+add;
				op[index++] = add;
				push(num);
				break;
			case 2:
				printf("Enter num to sub: ");
				scanf("%d",&add);
				num = num-add;
				push(num);
				op[index++] = add;
				break;
			case 3:
				display();
				break;
			case 4:
				status = undo();
				if (status != -1) 
					printf("After Undo A = %d\n",status);
				else
					printf("No undo operation\n");
				break;
			case 5:
				status = redo();
				if (status != -1) 
					printf("After redo A = %d\n",status);
				else
					printf("No redo operation\n");
				break;

		}
		printf("Do you want to continue(y/n): ");
		scanf(" %c",&c);
	}while(c=='y' || c =='Y');
}
