/*
72.Write a program which accept two doubly linear linked list from user and concat first N elements of source linked list after destination linked list.
Function Prototype : int ConcatFirstN( struct node **Src, struct node **Dest, int no );
Input source linked list : |30|<=>|30|<=>|70|
Input destination linked list : |10|<=>|20|<=>|30|<=>|40|
Input number of elements : 2
Output destination list : |10|<=>|20|<=>|30|<=>|40|<=>|30|<=>|30|
*/

#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};

int getLength(node* head) {
	int count = 0;
	node* temp = head;
	while(temp!= NULL) {
		temp = temp->next;
		count++;
	}
	return count;

}

//--------------------- getData() -------------------------- 
node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

//--------------------- createList() -------------------------- 
void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data " << i+1 << ": ";
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}	
	}
}
int display( struct node *head ) {
	node* temp = head;
	if(head == NULL) 
		std::cout << "\t ******List is Empty ******* " ;
	else {
	  while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != NULL)
			std::cout << "--->";
		
		temp =temp->next;
	  }
	}
	std::cout << std::endl << std::endl;


}

//---------------------------concat src to the dest ------------------------------
int concatFirstN( struct node **srcHead, struct node **destHead ,int no) {
		
		node* tempSrc ;
		int count =1;
		if(*srcHead == NULL)				//if src is head then return 
			return 0;
		
		if(*destHead == NULL ) {
						
			tempSrc = *srcHead;				//destHead is NULL then directly assign to the head 
			*destHead  = *srcHead;

			while(count < no) {
				tempSrc = tempSrc->next;
				count++;
			}
			tempSrc->next = NULL;			//make next link null
		}

		else {
			tempSrc = *srcHead;				 
			node* tempDest = *destHead;	

			while(tempDest->next != NULL)
				tempDest= tempDest->next;
			tempDest->next = tempSrc;
			
			while(count < no)	{
				tempSrc = tempSrc->next;
				count++;
			}
			tempSrc->next = NULL;			//break the link
		}
		return 1;


					
}

int main() {

	//take  two head ptr for two lists 
	struct node *src= NULL , *dest = NULL;

	//to take lenght and num
	int len , num;

	std::cout << "Enter number of elements in Source list: ";
	std::cin >> len;

	// ---- Test case 0 ----
	while(len < 0)  {
		std::cout << "Wrong value entered!!!\nPlease enter positive interger only " << std::endl;
		std::cout << "Enter again: ";
		std::cin >> len;
	}


	//create src list
	createList(&src, len);

	std::cout << "Enter number of elements in Destination list: ";
	std::cin >> len;

	// ---- Test case 0 ----
	while(len < 0)  {
		std::cout << "Wrong value entered!!!\nPlease enter positive interger only " << std::endl;
		std::cout << "Enter again: ";
		std::cin >> len;
	}
	//create dest list
	createList(&dest, len);

	//display all lists
	std::cout << "Source list -----------" <<std::endl;
	display(src);
	
	std::cout << "Destination list -----------" <<std::endl;
	display(dest);
	std::cout << std::endl;
	//concate list 
	
	int pos ;
	std::cout <<"Enter number of nodes for concatnation: "<<std::endl;
	std::cin >> pos;

	int max = getLength(src);		//find maximum of length

	//check whether entered pos is present in the list or not
	//If pos is greater maximum lenght or if it is less than 0 then it is invalid pos or
	while( pos < 0 || pos > max) {

		std::cout << "Entered position is not present in list. Length of list " <<getLength(src)<< " \nPlease enter valid position: "<<std::endl;		      std::cin >> pos;
	}

	int status = concatFirstN(&src , &dest, pos);

	std::cout << "After concatention ------------" <<std::endl;
	if(status) {
		std::cout << "Concatenated Successfully " <<std::endl;
		display(dest);
	}
	else {
		std::cout << "Source list is Empty" <<std::endl;
		display(dest);
	}
	return 0;


}

