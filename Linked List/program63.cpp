/*
63.Write a program which remove first node from doubly linear linked list.
Function Prototype : int DeleteFirst(
struct node **Head
);
Input linked list : |10|<=>|20|<=>|30|<=>|40|<=>|50|
Output linked list : |20|<=>|30|<=>|21|<=>|40|<=>|50|
*/


#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};


int getLength(node *head) {
	int count=0;
	node *temp =head;
	while(temp != NULL) {
		count++;
		temp = temp->next;
	}
	return count;
}
node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data: " << std::endl;
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}	
	}
}

void display(node *head) {
	node* temp = head;
	while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != NULL)
			std::cout << "--->";
		
		temp =temp->next;
	}
	std::cout << std::endl << std::endl;


}

int deleteFirst(node ** head) {
	
	node *temp = *head;
	if(*head == NULL) {
		return 0;
	}
	if(temp->next ==NULL) {
		
			*head =NULL;
			free(temp);
	}
	else {
		*head = temp->next;
		temp->next->prev = NULL;
		free(temp);
	}

}
int main() {

	node *head= NULL;
	int pos;

	int noOfNodes;
	std::cout << "Enter number of node: ";
	std::cin >> noOfNodes;
	//create doubly list
	createList(&head, noOfNodes);	
	display(head);

	int data;
	deleteFirst(&head);
	display(head);
	return 0;


}

