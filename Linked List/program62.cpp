/*
62.Write a program which add new node in doubly linear linked list at specified position.Function Prototype : int InsertAtPosition(
struct node **Head, int no, int pos
);
Input linked list : |20|<=>|30|<=>|40|<=>|50|<=>|60|
Input data element : 21
Input position : 3
Output linked list :|20|<=>|30|<=>|21|<=>|40|<=>|50|<=>|60|
*/


#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};


int getLength(node *head) {
	int count=0;
	node *temp =head;
	while(temp != NULL) {
		count++;
		temp = temp->next;
	}
	return count;
}
node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data: " << std::endl;
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}	
	}
}

void display(node *head) {
	node* temp = head;
	while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != NULL)
			std::cout << "--->";
		
		temp =temp->next;
	}
	std::cout << std::endl << std::endl;


}

int insertAtPos(node ** head , int data, int pos) {
	
		int len = getLength(*head);
		
		node *newNode = getData(data);
		int count = 1;
		if(*head==NULL) 
			*head = newNode;
		
		else if(pos > len ) {
			std::cout << "You have entered position greater than length of list .Therefore node will be added at the end " << std::endl;
			node *temp = *head;
			while(temp->next != NULL) 
				temp = temp->next;
			newNode->prev = temp;
			temp->next = newNode;

		}
		else {
			node *temp = *head;
			while(count < pos-1) {
				temp =temp->next;
				count++;
			}
			newNode->prev = temp;
			newNode->next = temp->next;
			temp->next = newNode;
		}

}

int main() {

	node *head= NULL;
	int pos;

	int noOfNodes;
	std::cout << "Enter number of node: ";
	std::cin >> noOfNodes;
	//create doubly list
	createList(&head, noOfNodes);	
	display(head);

	int data;
	std::cout << "ENter position to insert the new node: ";
	std::cin >> pos;
	while(pos < 0) {
				std::cout << "You have entered Negative value \n Please enter appropriate data: ";
				std::cin >> pos ;
	}
	std::cout << "Enter data to insert at " << pos <<" position: ";
	std::cin >> data;

	int len = getLength(head);
	//std::cout << "Length = " << len <<std::endl;


	insertAtPos(&head , data, pos);
	display(head);

	return 0;


}

