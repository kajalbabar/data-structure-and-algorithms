#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};

int getLength(node* head) {
	
	int count= 0;
	node* temp = head;

	if(head== NULL)
		return 0;
	do {
		count++;
		temp = temp->next;

	}while(temp!=head);
	return count;

}
node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

node* createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data: ";
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
			temp->next = *head;

		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
			temp->next = *head;
		}	
	}
	return temp;
}

int display( struct node *head ) {
	node* temp = head;
	if(head == NULL)
		std::cout << "\t ******List is Empty ******* " ;
	else {
	    do{
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != head)
			std::cout << "<=>";
		
		temp = temp->next;

	  }while(temp != head);
	}	

	std::cout << std::endl << std::endl;


}

//insert an element at the specifice node
int insertAtPos(node **head , node **tail,int data, int pos) {

		int len = getLength(*head);

		node *newNode = getData(data);
		int count = 1;
		if(*head==NULL)
			*head = newNode;

		else if(pos > len ) {
			std::cout << "You have entered position greater than length of list .Therefore node will be added at the end " << std::endl;
			
			node *temp = *tail;
			newNode->prev = temp;
			temp->next = newNode;
			newNode->next = *head;

		}
		else {
			node *temp = *head;
			while(count < pos-1) {
				temp =temp->next;
				count++;
			}
			newNode->prev = temp;
			newNode->next = temp->next;
			temp->next = newNode;

			if(temp->next ==*head)
				newNode->next = *head;
		}

}

int main() {

	node *head= NULL, *tail = NULL;
	int pos,data;

	int noOfNodes;
	std::cout << "Enter number of node: ";
	std::cin >> noOfNodes;

	//create doubly list
	tail = createList(&head, noOfNodes);
	std::cout << "Input List: ";	
	display(head);


	
	
	std::cout << "ENter position to insert the new node: ";
	std::cin >> pos;
	while(pos < 0) {
				std::cout << "You have entered Negative value \n Please enter appropriate data: ";
				std::cin >> pos ;
	}
	std::cout << "Enter data to insert at " << pos <<" position: ";
	std::cin >> data;

	std::cout << "After insertion at "<< pos << " position\n";
	insertAtPos(&head , &tail ,data, pos);
	display(head);
	
	return 0;


}

