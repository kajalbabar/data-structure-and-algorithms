/*
85.Write a program which accept source doubly linear linked list from user and copy the
contents into destination doubly linear
linked in descending order.
Function Prototype : int CopyDsc(struct node **Src, struct node **Dest );
*/

#include <iostream>

//structure 
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};

int getLength(node* head) {
	int count = 0;
	node* temp = head;
	while(temp!= NULL) {
		temp = temp->next;
		count++;
	}
	return count;

}

//--------------------- getData() --------------------------
node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}

//--------------------- createList() --------------------------
void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data " << i+1 << ": ";
		std::cin >> data;
		newNode = getData(data);

		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}
	}
}
int display( struct node *head ) {
	node* temp = head;
	if(head == NULL)
		std::cout << "\t ******List is Empty ******* " ;
	else {
	  while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;

		if(temp->next != NULL)
			std::cout << "--->";

		temp =temp->next;
	  }
	}
	std::cout << std::endl << std::endl;


}

//copy in asceding order
int CopyDesc( struct node **src , struct node **dest) {
		
        	//take all src list into an array
        	int len= getLength(*src);
        	int arr [len] , i=0;
        	
		struct node* tempSrc = *src;
		node* tempDest = *dest;
		while (tempSrc!= NULL){
			arr[i++] = tempSrc->no;
			tempSrc = tempSrc->next;
		}

		//sort data
	   	for(int i=0; i<len ;i++) {
			for(int j=i;j<len;j++) {
				if(arr[i] <= arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] =temp;
				}

			}
		}

		for(int i=0; i<len; i++) {

			node *newNode = getData(arr[i]);
			newNode->next = NULL;
			newNode->prev = NULL;
			
			if(*dest == NULL) {
				*dest =newNode;
				tempDest = newNode;
			}else {
				newNode->prev = tempDest;
				tempDest->next = newNode;
				tempDest = newNode;
			}
		}
}

int main() {

	  //head of src and dest link list
        struct node *srcHead= NULL;
        struct node *destHead = NULL;
        int src, dest ,data;

        std::cout << "Enter number of nodes in the source list: " ;
        std::cin >> src;

        createList(&srcHead,src);
       
       	//display src and dest lists
        std::cout << "\nSource List-----------------" << std::endl;
        display(srcHead);

	//call concate fun
        int status = CopyDesc(&srcHead, &destHead);
        std::cout << "Array after sorting in desceding order" <<std::endl;
        display(destHead);
	return 0;
}
