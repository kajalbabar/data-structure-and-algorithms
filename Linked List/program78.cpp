/*
78.Write a program which copies contents of source doubly linear linked list to destination
doubly linear linked list
which lies between the particular range which is accepted from user.
Function Prototype : int LLCopyRange( struct node **Src ,struct node **Dest , int start, int end)
*/


#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};

int getLength(node* head) {
	int count = 0;
	node* temp = head;
	while(temp!= NULL) {
		temp = temp->next;
		count++;
	}
	return count;

}

//--------------------- getData() -------------------------- 
node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

//--------------------- createList() -------------------------- 
void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data " << i+1 << ": ";
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}	
	}
}
int display( struct node *head ) {
	node* temp = head;
	if(head == NULL) 
		std::cout << "\t ******List is Empty ******* " ;
	else {
	  while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != NULL)
			std::cout << "--->";
		
		temp =temp->next;
	  }
	}
	std::cout << std::endl << std::endl;


}

int LLCopyRange(node **src , node **dest, int start, int end) {

	node *tempDest = *dest;
	node *tempSrc = *src;
	node* newNode = NULL;

	int diff = end - start;
	if(*src == NULL)
		return 1;
	else {
		while(start-- > 1) {
			tempSrc = tempSrc->next;
		}

		while(diff--) {
			newNode = getData(tempSrc->no);
			
			if(*dest == NULL){
				*dest = newNode;
				tempDest = newNode;
			}else {
				tempDest->next = newNode;
				newNode->prev = tempDest;
				tempDest = newNode;
			}
			tempSrc = tempSrc->next;
		 }	

	}
	return 0;
}

					

int main() {

	//take  two head ptr for two lists 
	struct node *src= NULL , *dest = NULL;

	//to take lenght and num
	int len , num;

	std::cout << "Enter number of elements in Source list: ";
	std::cin >> len;

	// ---- Test case 0 ----
	while(len < 0)  {
		std::cout << "Wrong value entered!!!\nPlease enter positive interger only " << std::endl;
		std::cout << "Enter again: ";
		std::cin >> len;
	}


	//create src list
	createList(&src, len);



	//display all lists
	std::cout << "Source list -----------" <<std::endl;
	display(src);
	
	std::cout << "Destination list -----------" <<std::endl;
	display(dest);
	std::cout << std::endl;
	
	int startcpyNode;
	std::cout << "Enter starting of node for copy: ";
	std::cin >> startcpyNode;	
	
	int endcpyNode;
	std::cout << "Enter ending of node for copy: ";
	std::cin >> endcpyNode;	

	int status = LLCopyRange(&src , &dest, startcpyNode, endcpyNode);

	std::cout << "After copy destination list ------------" <<std::endl;
	if(!status) {
		std::cout << "Copied Successfully " <<std::endl;
		display(dest);
	}
	else {
		std::cout << "Source list is Empty" <<std::endl;
		display(dest);
	}
	return 0;


}

