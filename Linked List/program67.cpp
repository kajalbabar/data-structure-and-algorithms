/*
67.Write a program which search first occurrence of particular element from doubly linear
linked list.
Function should return position at which element is found.
Function Prototype : int SearchFirstOcc(
struct node *Head , int no
);
Input linked list : |10|<=>|20|<=>|30|<=>|40|<=>|50|
Input element : 30
Output : 3
*/


#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};


node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data: " << std::endl;
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}	
	}
}
int display( struct node *head ) {
	node* temp = head;
	if(head == NULL) 
		std::cout << "\t ******List is Empty ******* " ;
	else {
	  while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != NULL)
			std::cout << "--->";
		
		temp =temp->next;
	  }
	}
	std::cout << std::endl << std::endl;


}

int searchFirstOcc(node* head, int num) {
		node* temp = head;
		int count=1;
		if(head == NULL) 
			return 0;

		while(temp != NULL) {
			if(temp->no == num)
				return count;
			temp = temp->next;
			count++;
		}
		return -1;

}


int main() {

	node *head= NULL;
	int pos;

	int noOfNodes;
	std::cout << "Enter number of node: ";
	std::cin >> noOfNodes;
	//create doubly list
	createList(&head, noOfNodes);
	std::cout << "Input List: ";	
	display(head);

	int data;
	std::cout << "Enter data to search: ";
	std::cin >> data;
	int status = searchFirstOcc(head , data);
	if(status == -1) 
		std::cout << "Entered element is not present in list" << std::endl;
	else if(status == 0)
		std::cout << "No any element found" << std::endl;
	else 
		std::cout << "First occurance of element " << data << " is found at position " << status << std::endl;


	return 0;


}

