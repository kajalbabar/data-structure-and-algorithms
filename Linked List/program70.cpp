/*
70.Write a program which searches all occurrence of particular element from doubly linear
linked list.
Function should return number of occurrence of that element.
Function Prototype : int SearchAll(
struct node *Head, int no
);
Input linked list : |10|<=>|20|<=>|30|<=>|40|<=>|30|<=>|30|
Input element : 30
Output : 3
*/


#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};


node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data " << i+1 << ": ";
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}	
	}
}
int display( struct node *head ) {
	node* temp = head;
	if(head == NULL) 
		std::cout << "\t ******List is Empty ******* " ;
	else {
	  while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != NULL)
			std::cout << "--->";
		
		temp =temp->next;
	  }
	}
	std::cout << std::endl << std::endl;


}

//------------------------find first occurace of element ---------
int searchAll(struct node *head, int no) {
		
		int  allOcc = 0;		
		node* temp;	
		temp = head;		//assign to head
		if (head == NULL)
			return -1;
		while(temp != NULL) {
			//if element found increment allOcc counter
			if(temp->no == no)
				allOcc++;		
			temp = temp->next;		//move to next node			

		}		
		return allOcc;	
}//end of SreachFirst

int main() {

	node *head= NULL;
	int pos;

	int noOfNodes;
	std::cout << "Enter number of node: ";
	std::cin >> noOfNodes;
	//create doubly list
	createList(&head, noOfNodes);
	std::cout << "Input List: ";	
	display(head);

	int data;
	std::cout << "Enter data to search: ";
	std::cin >> data;
	int status = searchAll(head , data);
	
	//check found or not
	if(status > 0)
		std::cout<< data << " is found in the list  " << status << " times"<< std::endl;
	else if(status == -1)
		std::cout << "No any element found in the list" << std::endl;
	else
		std::cout << "Element Not found in the list!!!!" <<std::endl;
	return 0;


}

