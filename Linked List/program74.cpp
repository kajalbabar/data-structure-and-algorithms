/*
74.Write a program which accepts two doubly linear linked list from user and also accept
range and concat elements of source
singly linear linked list from that range after doubly linear destination linked list.
Function Prototype : int ConcatListRange(struct node **Src , struct node **Dest, int start , int end);
Input source linked list : |30|<=>|30|<=>|70|<=>|80|<=>|90|<=>|100|
Input destination linked list : |30|<=>|40|
Input starting range : 2
Input ending range : 5
Output destination list:|30|<=>|40|<=>|30|<=>|70|<=>|80|<=>|90|
*/

#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};

int getLength(node* head) {
	int count = 0;
	node* temp = head;
	while(temp!= NULL) {
		temp = temp->next;
		count++;
	}
	return count;

}

//--------------------- getData() -------------------------- 
node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

//--------------------- createList() -------------------------- 
void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data " << i+1 << ": ";
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}	
	}
}
int display( struct node *head ) {
	node* temp = head;
	if(head == NULL) 
		std::cout << "\t ******List is Empty ******* " ;
	else {
	  while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != NULL)
			std::cout << "--->";
		
		temp =temp->next;
	  }
	}
	std::cout << std::endl << std::endl;


}

//---------------------------concat src to the dest ------------------------------
int concatListRange(  node **srcHead,  node **destHead ,int start, int end) {
		
		node* tempSrc = *srcHead;
		node* tempDest = *destHead;
		
		int count = 1 ;
		int pos  = getLength(*srcHead) - start;
		
		if(*srcHead == NULL)				//if src is head then return 
			return 0;
		
		if(*destHead == NULL ) {
			while(start--) 
				tempSrc = tempSrc->next;
			*destHead = tempSrc;
			
			while(end--) 
				tempSrc = tempSrc->next;
			tempSrc->next = NULL;
		}

		else {
			//reach end to the dest node
			while(tempDest->next != NULL)
				tempDest= tempDest->next;
			
			while(start-- > 1) 
				tempSrc = tempSrc->next;
			
			tempDest->next = tempSrc;
			
			tempSrc = *srcHead;
			while(end-- > 1) 
				tempSrc = tempSrc->next;
			tempSrc->next = NULL;			//break the link
		}
		return 1;


					
}

int main() {

	//take  two head ptr for two lists 
	struct node *src= NULL , *dest = NULL;

	//to take lenght and num
	int len , num;

	std::cout << "Enter number of elements in Source list: ";
	std::cin >> len;

	// ---- Test case 0 ----
	while(len < 0)  {
		std::cout << "Wrong value entered!!!\nPlease enter positive interger only " << std::endl;
		std::cout << "Enter again: ";
		std::cin >> len;
	}


	//create src list
	createList(&src, len);

	std::cout << "Enter number of elements in Destination list: ";
	std::cin >> len;

	// ---- Test case 0 ----
	while(len < 0)  {
		std::cout << "Wrong value entered!!!\nPlease enter positive interger only " << std::endl;
		std::cout << "Enter again: ";
		std::cin >> len;
	}
	//create dest list
	createList(&dest, len);

	//display all lists
	std::cout << "Source list -----------" <<std::endl;
	display(src);
	
	std::cout << "Destination list -----------" <<std::endl;
	display(dest);
	std::cout << std::endl;
	//concate list 
	
	int start, end ;
	std::cout <<"Enter starting position for concatnation: "<<std::endl;
	std::cin >> start;

	std::cout <<"Enter ending position for concatnation: "<<std::endl;
	std::cin >> end;

	int max = getLength(src);		//find maximum of length

	//check whether entered pos is present in the list or not
	//If pos is greater maximum lenght or if it is less than 0 then it is invalid pos or
	while( start < 0 || end > max) {

		std::cout << "Entered position is not present in list. Length of list " <<getLength(src)<< " \nPlease enter valid position: "<<std::endl;		      
		std::cin >> start;
	}

	int status = concatListRange(&src , &dest, start,end);

	std::cout << "After concatention ------------" <<std::endl;
	if(status) {
		std::cout << "Concatenated Successfully " <<std::endl;
		display(dest);
	}
	else {
		std::cout << "Source list is Empty" <<std::endl;
		display(dest);
	}
	return 0;


}

