/*
 * 82.Write a program which accept source doubly linear linked list and destination doubly
linear linked list and
check whether source list is sub list of destination list. Function returns first position
at which sub list found.
Function Prototype : int SubList(
struct node **Src, struct node **Dest
);
*/

#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};

//--------get length function-----------
int getLength(node* head) {
	int count = 0;
	node* temp = head;
	while(temp!= NULL) {
		temp = temp->next;
		count++;
	}
	return count;

}


node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}

void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data " <<i+1 << ": " ;
		std::cin >> data;
		newNode = getData(data);

		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}
	}
}

void display(node *head) {
	node* temp = head;
	while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;

		if(temp->next != NULL)
			std::cout << "<=>";

		temp =temp->next;
	}
	std::cout << std::endl << std::endl;


}

//find sub list function
int SubList(struct node **src, struct node **dest ) {
	
		//temp variable for traversal
		struct node *srcTemp = *src;
		struct node *destTemp = *dest;

		//calculate length of the lists
		int srcLen = getLength(*src);
		int destLen = getLength(*dest);
		
		//if length length if src is less then return
		if(srcLen > destLen)
			return -1;
		int countSub=0,start, count=0;	//countSub of track the matching char

	
		
		while(destTemp!= NULL && srcTemp !=NULL) {
				count++;
				if(srcTemp->no == destTemp->no) {	
					//std::cout << "*****\n";
					countSub++;
					srcTemp = srcTemp->next;	
					if(countSub == 1) 
						start = count;	//to get starting postion of substring
				}else {
					srcTemp =*src;
					countSub=0;
				}
				destTemp = destTemp->next;
		}
			
		//std::cout << "*****\n";
		
		if(countSub == srcLen) 
			return start;
		else
			return 0;
 
}//end of fun



int main() {

	node *src= NULL;
	node *dest= NULL;
	int noOfNodes;
	
	std::cout << "Enter number of node in source list: ";
	std::cin >> noOfNodes;
	
	//Test case 0:- Enter positive length of list
	while(noOfNodes < 0) {
		std::cout << "Please enter positive value: ";
		std::cin >> noOfNodes ;
	}

	//create source list
	createList(&src, noOfNodes);

	std::cout << "Enter number of node in destination list: ";
        std::cin >> noOfNodes;

	//Test case 0:- Enter positive length of list
        while(noOfNodes < 0) {
                std::cout << "Please enter positive value: ";
                std::cin >> noOfNodes ;
        }
        //create source list
        createList(&dest, noOfNodes);

        std::cout << "\t************ Input lists *************" <<std::endl;
	std::cout << "Source list: " ;
	display(src);

	std::cout << "Destination list: " ;
	display(dest);
	

	//call concat fun
	int status = SubList(&src, &dest); 
	if(status > 0)  {
		std::cout << "\nSub List is present in the destinaiton list at location " << status << std::endl;
	}
	else if(status == -1)
		std::cout << "\nLength of the source list is greater than the destination. " << std::endl;  
	else 
		std::cout << "source is not sublist of destination" << std::endl;	
	return 0;


}

