/*
87.Write a program which add new node in singly circular linked list at last position.
Function Prototype : int InsertLast(
struct node **Head, struct node **Tail, int
no
);
*/


#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};


node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

node* createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data: ";
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
			temp->next = *head;

		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
			temp->next = *head;
		}	
	}
	return temp;
}

int display( struct node *head ) {
	node* temp = head;
	if(head == NULL)
		std::cout << "\t ******List is Empty ******* " ;
	else {
	    do{
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != head)
			std::cout << "<=>";
		
		temp = temp->next;

	  }while(temp != head);
	}	

	std::cout << std::endl << std::endl;


}

int insertAtLast(node **head , node **tail,int data) {
	
	//create newNode	
	node *newNode = getData(data);
	node *temp = *head;;	
	
	if(*head == NULL) {
		*head = newNode;
		newNode->next = *head;
	}
	else {
		newNode->prev = *tail;
		(*tail)->next =  newNode;		
		newNode->next = *head; //make link to the first node
	}
}

int main() {

	node *head= NULL, *tail = NULL;
	int pos,data;

	int noOfNodes;
	std::cout << "Enter number of node: ";
	std::cin >> noOfNodes;

	//create doubly list
	tail = createList(&head, noOfNodes);
	std::cout << "Input List: ";	
	display(head);

	std::cout << "Enter the new data insert at last position: ";
	std::cin >> data;

	insertAtLast(&head,&tail,data);
	
	std::cout << "After insertion at last position\n";
	display(head);
	return 0;


}

