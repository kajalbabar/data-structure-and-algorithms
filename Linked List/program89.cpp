

#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};

int getLength(node* head) {
	
	int count= 0;
	node* temp = head;

	if(head== NULL)
		return 0;
	do {
		count++;
		temp = temp->next;

	}while(temp!=head);
	return count;

}
node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

node* createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data: ";
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
			temp->next = *head;

		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
			temp->next = *head;
		}	
	}
	return temp;
}

int display( struct node *head ) {
	node* temp = head;
	if(head == NULL)
		std::cout << "\t ******List is Empty ******* " ;
	else {
	    do{
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != head)
			std::cout << "<=>";
		
		temp = temp->next;

	  }while(temp != head);
	}	

	std::cout << std::endl << std::endl;


}

int deleteFirst(node** head, node **tail) {
	node* temp= *head;

	if(*head==NULL)
		return 0;
	
	if(temp->next == *head){
		*head = NULL;
		free(temp);
	}
	else {
		temp = *head;
		*head = temp->next;
		(*head)->prev = NULL;
		(*tail)->next = *head;
		free(temp);
	}


}

int main() {

	node *head= NULL, *tail = NULL;
	int pos,data;

	int noOfNodes;
	std::cout << "Enter number of node: ";
	std::cin >> noOfNodes;

	//create doubly list
	tail = createList(&head, noOfNodes);
	std::cout << "Input List: ";	
	display(head);


	
	
	deleteFirst(&head, &tail);
	std::cout << "After deletion of first position "<<std::endl;
	deleteFirst(&head , &tail);
	display(head);
	return 0;


}

