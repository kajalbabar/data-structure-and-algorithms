/*
65.Write a program which remove node from doubly linear linked list which is at specified
position.
Function Prototype : int DeleteAtPosition(struct node **Head, int pos
);
Input linked list : |10|<=>|20|<=>|30|<=>|40|<=>|50|
Input position : 4
Output linked list : |10|<=>|20|<=>|30|<=>|50|
*/


#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};


int getLength(node *head) {
	int count=0;
	node *temp =head;
	while(temp != NULL) {
		count++;
		temp = temp->next;
	}
	return count;
}
node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data: " << std::endl;
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}	
	}
}

void display(node *head) {
	node* temp = head;
	while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != NULL)
			std::cout << "--->";
		
		temp =temp->next;
	}
	std::cout << std::endl << std::endl;


}

int deletePos(node ** head ,  int pos) {
	
		int len = getLength(*head);
		node *temp = *head;
		
		int count = 1;
		if(*head==NULL) 
			return 0;
		if(pos == 1){
			*head = temp->next;
			free(temp);
		}	
				
		else {
			while(count < pos-1) {
				temp = temp->next;
				count++;
			}
			node* temp2 = temp->next;
			temp->next = temp2->next;
			temp2->next->prev = temp;
			free(temp2);	
		}

}

int main() {

	node *head= NULL;
	int pos;

	int noOfNodes;
	std::cout << "Enter number of node: ";
	std::cin >> noOfNodes;
	//create doubly list
	createList(&head, noOfNodes);	
	display(head);

	std::cout << "ENter position to delete the exiting node: ";
	std::cin >> pos;
	int len = getLength(head);


	while(pos < 0 || pos > len) {
				if(pos < 0) 
					std::cout << "You have entered Negative value \n";
				else
					std::cout << "You have entered position which is not present in list(lenght = " << len <<")\n ";
					
				std::cout << "Enter position again: ";
				std::cin >> pos ;
	}

	deletePos(&head , pos);
	display(head);

	return 0;


}

