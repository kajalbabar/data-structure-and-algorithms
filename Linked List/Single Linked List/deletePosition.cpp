/*
 * 91.Write a program which remove node from singly circular linked list which is at specified
position.
Function Prototype : int DeleteAtPosition(
struct node **Head, struct node **Tail, int
pos);
*/

int deletePos(node **head, node **tail , int pos) {
	
	node *temp1 = *head, *temp2;
 
	int len = getLength(*head);
	if(*head == NULL)
		return 0;
	
	else {
		int count = 1;
		while(count++ < pos-1)
			temp1 = temp1->next;
		
		temp2 = temp1->next;
		if(temp2->next == *head ) {
			temp1->next = NULL;
			free(temp2);	
		}
		else {
			temp2->next->prev = temp1;
			temp1->next = temp2->next;
			free(temp2);
		}
	}
	return 1;
}
