/*
98.Write a program which accept two doubly circular linked list from user and concat first N
elements of source linked list
after destination linked list.
Function Prototype : int ConcatFirstN(
struct node **SrcHead, struct node **SrcTail,
struct node **DestHead, struct node **DestTail, int no
);
*/

int concatFirstN(node **srcHead , node **srcTail, node **destHead, node **destTail, int no) {
		
		node *tempSrc = *srcHead, *tempDest = *destHead;

		if(*srcHead == NULL)	
			return 0;


		if(*destHead == NULL) {
			
			*destHead = *srcHead;
			
			while(no--) {
				tempSrc = tempSrc->next;
			}	
			
			node *delNode = tempSrc->next, *prevNode;
			tempSrc->next = *destHead;				//discconnect further links and link to the first node of dest list
			
			/* free the next further nodes
			 * 	first traverse upto last node
			 *	store address of previos node in prevNode
			 * 	move delNode to the next
			 * 	free prevNode 
			 */
			while(delNode->next != *srcHead)  {
					
					prevNode = delNode;
					delNode = delNode->next;
					free(prevNode);

			}
		}

		else {
			(*tailDest)->next = *srcHead;				//link srcHead at the end of dest list 
			
			while(no--) {
				tempSrc = tempSrc->next;
			}	
			
			/*delNode : for moving 
			 * prevNode: to delete
			 */
			node *delNode = tempSrc->next, *prevNode;		
			
			tempSrc->next = *destHead;				//discconnect further links
			
			/* free the next further nodes
			 * 	first traverse upto last node
			 *	store address of previos node in prevNode
			 * 	move delNode to the next
			 * 	free prevNode 
			 */
			while(delNode->next != *srcHead)  {
					
					prevNode = delNode;
					delNode = delNode->next;
					free(prevNode);

			}
		}
		return 1;

}
