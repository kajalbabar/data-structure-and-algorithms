/*
 * 97.Write a program which accept two doubly circular linked list from user and concat source
linked list after destination
linked list.
Function Prototype : int ConcatList(
struct node **SrcHead, struct node **SrcTail,
struct node **DestHead, struct node **DestTail
);
*/

int concatList(node **srcHead , node **srcTail, node **destHead, node **destTail) {
		
		node *tempSrc = *srcHead, *tempDest = *destHead;
		if(*srcHead == NULL)	
			return 0;


		if(*destHead == NULL) {
			*destHead = *srcHead;
			(*srcTail)->next = *destHead;
		}

		else {
			(*tailDest)->next = *srcHead;		//link srcHead at the end of dest list 
			(*tailSrc)->next = *destHead;		//make a circular link
			/* add link from last node of src list to the first node of dest list 
			*/	
		}
		return 1;

}
