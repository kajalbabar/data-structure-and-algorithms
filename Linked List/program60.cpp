/*
 * 60.Write a program which add new node in doubly linear linked list at first position.
Function Prototype : int InsertFirst(
struct node **Head, int no
);
Input linked list : |20|<=>|30|<=>|40|<=>|50|<=>|60|
Input data element : 21
Output linked list : |21|<=>|20|<=>|30|<=>|40|<=>|50|<=>|60|
*/

#include <iostream>
struct node {
	int no;
	// Data element
	struct node *prev ,*next;		// Address of next node
};

node* getData(int data) {
	node * newNode = (node*) malloc (sizeof(node));
	newNode->no = data;
	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}	

void createList(node** head, int n) {

	int data;
	node *newNode, *temp =NULL;
	for(int i=0 ; i<n; i++){
		std::cout << "Enter data: " << std::endl;
		std::cin >> data;
		newNode = getData(data);
			
		if(*head == NULL) {
			*head = newNode;
			temp = *head;
		}
		else {
			temp->next = newNode;
			newNode->prev = temp;
			temp = newNode;
		}	
	}
}

void display(node *head) {
	node* temp = head;
	while(temp != NULL) {
		std::cout << "|"<<temp->no << "|" ;
		
		if(temp->next != NULL)
			std::cout << "--->";
		
		temp =temp->next;
	}
	std::cout << std::endl << std::endl;


}

int insertAtFirst(node ** head , int data) {
		node *newNode = getData(data);
		if(*head==NULL) 
			*head = newNode;
		else {
			newNode->prev = NULL;
			newNode->next = *head;
			*head = newNode;
		}
	

}

int main() {

	node *head= NULL;
	int noOfNodes;
	std::cout << "Enter number of node: ";
	std::cin >> noOfNodes;
	//create doubly list
	createList(&head, noOfNodes);	
	display(head);

	int data;
	std::cout << "Enter data to insert at first position: ";
	std::cin >> data;

	insertAtFirst(&head , data);
	display(head);

	return 0;


}

